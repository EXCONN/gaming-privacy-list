# EXCONN Gaming Privacy List
Block list to prevent tracking and analytics in video games and software used for gaming.
All domains were collected by the EXCONN team and members, while gaming on the video game digital distribution platform **Steam**.


## About
**Repository is used to make the block list easier accessible for DNS blocking application like Pi-hole or systemwide hosts file generator.**
- For more information please visit the current project page and Steam group [Exposed Connection](https://steamcommunity.com/groups/exposedconn).

## Lists
Use the following links for your automated DNS blocking application:
* [Main list](https://codeberg.org/EXCONN/gaming-privacy-list/raw/branch/main/EXCONN-main.txt) blocks the important tracking and analytics domains while not breaking functionality.
* [Full list](https://codeberg.org/EXCONN/gaming-privacy-list/raw/branch/main/EXCONN-full.txt) contains the main and optional domains, which may brake some applications functionality or result in missing side features. See comments.

## License
[CC BY-NC-SA 4.0](https://codeberg.org/EXCONN/gaming-privacy-list/src/branch/main/LICENSE)